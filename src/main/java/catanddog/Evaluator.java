package catanddog;

import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class Evaluator {

    private static final Logger logger = LoggerFactory.getLogger(Evaluator.class);

    private static final Path modelPath = Paths.get("./model.18.zip");
    private static final Path imagePath = Paths.get("./images/test2");

    public static void main(String[] args) throws IOException {
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(modelPath.toFile());

        ImageRecordReader testRecordReader = new ImageRecordReader(64, 64, 3, new CatAndDogLabelMaker());
        FileSplit testData = new FileSplit(imagePath.toFile(), NativeImageLoader.ALLOWED_FORMATS);
        testRecordReader.initialize(testData);

        DataSetIterator testDataIter = new RecordReaderDataSetIterator(testRecordReader, 200);
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
        scaler.fit(testDataIter);
        testDataIter.setPreProcessor(scaler);

        Evaluation eval = network.evaluate(testDataIter);
        logger.info("{}", eval.stats(false));
    }

}
