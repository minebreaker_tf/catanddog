package catanddog;

import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

@SuppressWarnings("FieldCanBeLocal")
public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final File trainIamgePath = new File("images/train");
    private static final File testImagePath = new File("images/test2");

    private static final int height = 64;
    private static final int width = 64;
    private static final int channels = 3;
    private static final int numLabels = 2;
    private static final int batchSize = 40;
    private static final int iterations = 1;

    private static final long seed = 123;
    private static final Random random = new Random(seed);

    public static void main(String[] args) throws IOException {

//        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
        CudaEnvironment.getInstance().getConfiguration()
                       .setMaximumDeviceCache(6L * 1024L * 1024L * 1024L);

        logger.info("Build model....");
        MultiLayerNetwork network = model();
        network.init();
        network.setListeners(new ScoreIterationListener(100), prepareUi());

        for (int i = 1; i < 101; i++) {
            logger.info("Train: {} times", i);
            train(network);
            evaluate(network);
            save(network, Paths.get("./model." + i + ".zip"));
        }
    }

    private static StatsListener prepareUi() {
        UIServer server = UIServer.getInstance();
        StatsStorage storage = new InMemoryStatsStorage();
        server.attach(storage);
        return new StatsListener(storage);
    }

    private static MultiLayerNetwork model() {
        return new MultiLayerNetwork(
                new NeuralNetConfiguration.Builder()
                        .seed(seed)
                        .miniBatch(true)
                        .weightInit(WeightInit.DISTRIBUTION)
                        .dist(new NormalDistribution(0.0, 0.01))
                        .activation(Activation.RELU)
                        .updater(Updater.NESTEROVS).momentum(0.9)
                        .iterations(iterations)
                        .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .learningRate(0.4)
                        .biasLearningRate(0.8)
//                        .learningRateDecayPolicy(LearningRatePolicy.Schedule)
//                        .learningRateSchedule(ImmutableMap.of(
//                                0, 0.02,
//                                300, 0.01,
//                                2000, 0.008
//                        ))
//                        .learningRateDecayPolicy(LearningRatePolicy.Step)
//                        .lrPolicyDecayRate(0.1)
//                        .lrPolicySteps(2000)
                        .regularization(true).l2(0.0005)
                        .list()
                        .layer(0, new ConvolutionLayer.Builder(new int[] { 11, 11 }, new int[] { 4, 4 }, new int[] { 3, 3 })
                                .name("cnn1")
                                .nIn(channels)
                                .nOut(96)
                                .biasInit(0.0)
                                .build())
                        .layer(1, new LocalResponseNormalization.Builder().name("lrn1").build())
                        .layer(2, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool1").build())
                        .layer(3, new ConvolutionLayer.Builder(new int[] { 5, 5 }, new int[] { 1, 1 }, new int[] { 2, 2 })
                                .name("cnn2")
                                .nOut(256)
                                .biasInit(1)
                                .build())
                        .layer(4, new LocalResponseNormalization.Builder().name("lrn2").build())
                        .layer(5, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool2").build())
                        .layer(6, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn3")
                                .nOut(384)
                                .biasInit(0.0)
                                .build())
                        .layer(7, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn4")
                                .nOut(384)
                                .biasInit(1)
                                .build())
                        .layer(8, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn5")
                                .nOut(256)
                                .biasInit(1)
                                .build())
                        .layer(9, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool3").build())
                        .layer(10, new DenseLayer.Builder()
                                .name("ffn1")
                                .nOut(4096)
                                .biasInit(1)
                                .dropOut(0.5)
                                .dist(new GaussianDistribution(0, 0.005))
                                .build())
                        .layer(11, new DenseLayer.Builder()
                                .name("ffn2")
                                .nOut(4096)
                                .biasInit(1)
                                .dropOut(0.5)
                                .dist(new GaussianDistribution(0, 0.005))
                                .build())
                        .layer(12, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                                .name("output")
                                .nOut(numLabels)
                                .activation(Activation.SOFTMAX)
                                .build())
                        .backprop(true)
                        .pretrain(false)
                        .setInputType(InputType.convolutional(height, width, channels))
                        .build());
    }

    private static void train(MultiLayerNetwork network) throws IOException {
        logger.info("Train model...");
        ImageRecordReader trainRecordReader = new ImageRecordReader(height, width, channels, new CatAndDogLabelMaker());
        FileSplit trainData = new FileSplit(trainIamgePath, NativeImageLoader.ALLOWED_FORMATS, random);
        trainRecordReader.initialize(trainData);

        DataSetIterator trainDataIter = new RecordReaderDataSetIterator(trainRecordReader, batchSize);
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
        scaler.fit(trainDataIter);
        trainDataIter.setPreProcessor(scaler);

        network.fit(trainDataIter);
    }

    private static void evaluate(MultiLayerNetwork network) throws IOException {
        logger.info("Evaluate model....");
        ImageRecordReader testRecordReader = new ImageRecordReader(height, width, channels, new CatAndDogLabelMaker());
        FileSplit testData = new FileSplit(testImagePath, NativeImageLoader.ALLOWED_FORMATS);
        testRecordReader.initialize(testData);

        DataSetIterator testDataIter = new RecordReaderDataSetIterator(testRecordReader, 200);
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
        scaler.fit(testDataIter);
        testDataIter.setPreProcessor(scaler);

        Evaluation eval = network.evaluate(testDataIter);
        logger.info("{}", eval.stats(false));
    }

    private static void save(MultiLayerNetwork network, Path saveTo) throws IOException {
        logger.info("Saving model...");
        ModelSerializer.writeModel(network, saveTo.toFile(), false);
    }

}
