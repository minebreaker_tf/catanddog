package catanddog;

import org.datavec.api.io.labels.PathLabelGenerator;
import org.datavec.api.writable.Text;
import org.datavec.api.writable.Writable;

import java.io.File;
import java.net.URI;

public final class CatAndDogLabelMaker implements PathLabelGenerator {
    private static final long serialVersionUID = 0L;

    @Override
    public Writable getLabelForPath(String path) {
        if (path.contains("dog")) {
            return new Text("dog");
        } else if (path.contains("cat")) {
            return new Text("cat");
        } else {
            throw new IllegalArgumentException(path);
        }
    }

    @Override
    public Writable getLabelForPath(URI uri) {
        return getLabelForPath(new File(uri).toString());
    }
}
